# Thinking in Swift
## #1: Value Types & Protocols

---

## Roadmap

- How and when to use force-unwrapping
- Why to prefer value types
- DEMO: Value Types
- Why to prefer composition over inheritence
- DEMO: Protocols

---

## Don't force `it!`

- Do NOT use force-unwrapping in general, or use them wisely.
- If a variable is `Optional`, there is a reason for it. 
- Use `if-let` and `guard` to handle errors properly.

---

## When to -occasionally- force `it!`

- `IBOutlets` are safe to unwrap. You know for sure that they will have a value just after init.
- If only a developer error can cause that value to be nil. Crash the app, and fix it ASAP.
- If a previous check assures that your value cannot be nil.

---

## What are value types?

Enums and structs are value types.

- Single ownership.
- They are easy to reason about.
- They are dead. (No side effects!)
- They are powerful. (enums, functions)
- They are testable.

---

## What are value types?

```

Object: Pablo, the dog
Value:  10


Object: Human 
Value:  "E"
```

---

## Prefer value types

```swift
class User { ... }

class ViewController {
	var user: User
}

class NetworkOperation {
	init(user: User)
}
```

---

## Prefer value types

```swift
struct User { ... }

class ViewController {
	var user: User
}

class NetworkOperation {
	init(user: User)
}
```

---

# DEMO
## Value Types

---

## Inheritence

![inline](game.png)

---

### Requirement:

## Castles & Zap Monsters can shoot.

---

## Inheritence :no_mouth:

![inline](game2.png)

---

# :thumbsdown:

---

## Protocol composition :thumbsup:

![inline](game-traits.png)

---

# DEMO
## Protocols
