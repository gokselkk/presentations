//: Playground - noun: a place where people can play

import Foundation

// ------------------------------------------------------
// MARK: Helper structs
// ------------------------------------------------------

public struct Point: CustomStringConvertible {
    
    public let x: Int
    public let y: Int
    
    public var description: String {
        return "(\(self.x), \(self.y))"
    }
}

public struct Projectile: CustomStringConvertible {
    
    public let position: Point
    public let damage: UInt
    
    public var description: String {
        return "\(position.description) | Dmg: \(damage)"
    }
}






























// ------------------------------------------------------
// MARK: Traits
// ------------------------------------------------------

public protocol GameObject: PositionTrait {
    
}

public protocol PositionTrait {
    
    var position: Point { get set }
}

public protocol GunTrait {
    
    var damage: UInt { get }
    func shoot() -> Projectile
}

public protocol MovementTrait {
    
    mutating func moveTo(_ point: Point)
}

public protocol HealthTrait {
    
    var health: UInt { get set }
    mutating func takeDamage(projectile: Projectile)
}






























// ------------------------------------------------------
// MARK: Trait default implementations
// ------------------------------------------------------

public extension HealthTrait {
    
    public mutating func takeDamage(projectile: Projectile) {
        self.health -= projectile.damage
    }
}

public extension GunTrait where Self: PositionTrait {
    
    public func shoot() -> Projectile {
        return Projectile(position: self.position, damage: self.damage)
    }
}

public extension MovementTrait where Self: PositionTrait {
    
    public mutating func moveTo(_ point: Point) {
        self.position = point
    }
}






























// ------------------------------------------------------
// MARK: Game objects
// ------------------------------------------------------

public struct Soldier: GameObject, HealthTrait, GunTrait, MovementTrait, CustomStringConvertible {
    
    public var position: Point
    public var health: UInt
    public var damage: UInt
}

public struct Tower: GameObject, HealthTrait, GunTrait, CustomStringConvertible {
    
    public var position: Point
    public var health: UInt
    public var damage: UInt
}






























// ------------------------------------------------------
// MARK: Debugging
// ------------------------------------------------------

public extension CustomStringConvertible where Self: protocol<GameObject, HealthTrait, GunTrait> {
    
    public var description: String {
        return "\(position.description) | HP: \(health) | Dmg: \(damage)"
    }
}






























// ------------------------------------------------------
// MARK: Game
// ------------------------------------------------------

var soldier = Soldier(position: Point(x: 0, y: 0), health: 100, damage: 10)
var tower = Tower(position: Point(x: 0, y:0), health: 1000, damage: 30)

// Shoot tower.
let projectile1 = soldier.shoot()
tower.takeDamage(projectile: projectile1)

// Move soldier.
soldier.moveTo(Point(x: 1, y: 1))

// Shoot tower.
let projectile2 = soldier.shoot()
tower.takeDamage(projectile: projectile2)






























