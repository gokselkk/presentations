
import Foundation

public struct Point {
    
    public var x: Int
    public var y: Int
    
    public init(x: Int, y: Int) {
        self.x = x
        self.y = y
    }
}

public enum Direction {
    case up
    case left
    case down
    case right
}

public struct Player {
    
    public let name: String
    public private(set) var coordinate: Point
    public private(set) var movementHistory: [Point] = []
    
    public init(name: String, coordinate: Point) {
        self.name = name
        self.coordinate = coordinate
        self.movementHistory.append(self.coordinate)
    }
}

// MARK: Mutating functions

extension Player {
    
    public mutating func move(_ direction: Direction) {
        switch direction {
        case .up:
            coordinate.y -= 1
        case .left:
            coordinate.x -= 1
        case .down:
            coordinate.y += 1
        case .right:
            coordinate.x += 1
        }
        movementHistory.append(coordinate)
    }
    
    public mutating func moveBack() {
        
        guard movementHistory.count > 1 else {
            return
        }
        
        movementHistory.removeLast()
        coordinate = movementHistory.last!
    }
}

// MARK: Functional mutating functions

extension Player {
    
    public func moving(_ direction: Direction) -> Player {
        var player = self
        player.move(direction)
        return player
    }
    
    public func movingBack() -> Player {
        var player = self
        player.moveBack()
        return player
    }
}

// MARK: Debugging

extension Point: CustomStringConvertible, CustomDebugStringConvertible {
    
    public var description: String {
        return "(\(x), \(y))}"
    }
    
    public var debugDescription: String {
        return description
    }
}

extension Player: CustomStringConvertible, CustomDebugStringConvertible {
    
    public var description: String {
        return "{\(name) in \(coordinate)}"
    }
    
    public var debugDescription: String {
        return description
    }
}
