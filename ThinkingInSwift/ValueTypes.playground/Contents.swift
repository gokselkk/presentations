//: Playground - noun: a place where people can play

import UIKit

// ----------------
// Pass by value
// ----------------

var inventory1 = ["bow", "axe", "sword"]
var inventory2 = inventory1

inventory2.append("arrow")

print(inventory1)
print(inventory2)































// ----------------
// Immutability
// ----------------

let garona = Player(name: "Garona", coordinate: Point(x: 0, y: 0))
// Cannot make mutating call `move` on a constant.
// garona.move(.up)
let movedGarona = garona.moving(.up)

var medivh = Player(name: "Medivh", coordinate: Point(x: 0, y: 0))
medivh.move(.up) // Valid. It's a variable.
let movedMedivh = medivh.moving(.up)































// ----------------
// Keeping history
// ----------------

var checkpoints: [Player] = []

var elf = Player(name: "Elf", coordinate: Point(x: 0, y: 0))

elf.move(.right)
elf.move(.down)

checkpoints.append(elf)

elf.move(.right)
elf.move(.up)

print("Movement history: \(elf.movementHistory)")

elf.moveBack()

// Start from previous checkpoint.
let youngElf = checkpoints.last!
print("Movement history: \(youngElf.movementHistory)")






























// ----------------
// Binding
// ----------------

var gnome = Player(name: "Gnome", coordinate: Point(x: 0, y: 0))
    .moving(.up)
    .moving(.down)
    .moving(.down)
    .moving(.down)
    .moving(.right)
    .moving(.right)






























