footer: Göksel Köksal
slidenumbers: true

# Text Validation
## Library

---

# Agenda

- Why?
- Basics
- DEMO

---

**ViewController.m**

```
- (BOOL)textField:(UITextField *)textField 
	shouldChangeCharactersInRange:(NSRange)range 
	replacementString:(NSString *)string
{
	if (textField == self.usernameTextField)
	{
		NSString *text = [textField.text 
			stringByReplacingCharactersInRange:range 
			withString:string];
		return (text.length < 10);
	}
	else if 
	{
		...
	}
}
``` 

---

**ViewController.m**

```objectivec
- (BOOL)textField:(UITextField *)textField 
	shouldChangeCharactersInRange:(NSRange)range 
	replacementString:(NSString *)string
{
	if (textField == self.usernameTextField)
	{
		NSString *text = [textField.text 
			stringByReplacingCharactersInRange:range 
			withString:string];
		return (text.length < 10);
	}
	else if 
	{
		...
	}
}
``` 

![](norton.gif)

---

# Why is it so bad?

:facepunch: View controller should **not** contain any logic.

:facepunch: Validation logic is **not** reusable.

:point_right: Only this part is relevant: ```(text.length < 10)```

:facepunch: Requirements are **not** clear.

---

![fit](TextValidationDiagram.png)

---

# Basics

- **Form Validator**: Validates a form that contains multiple fields.
- **Field Validator**: Validates a single field.
- **Text Validation Rule**: Validates text for a single rule.
- **Generic Validation Rule**: Validates anything.

---

# Text Validation Rule
## Example

Rule for max length:

```objectivec
[[PZTTextValidationRule alloc] initWithBlock:BOOL^(NSString *string) {
	return [string length] <= 10;
}];
```

---

# Generic Validation Rule
## Example

Rule for text equality check between two fields:

```objectivec
[[PZTGenericValidationRule alloc] initWithBlock:BOOL^() {
	return [self.passwordField.text isEqualToString:self.rePasswordField.text];
}];
```

---

# Field Validator

Keeps collection of rules to validate a field.

- **Typing**: Rules to be satisfied while user is typing.
- **Proceed**: Rules to be satisfied to proceed with next field.
- **Submit**: Rules to be satisfied to enable submit button.
- **Acceptance**: Rules to be satisfied before sending server.

---

# Form Validator

- Maps validators to fields.
- Keeps ```CanSubmit``` and ```CanAccept``` state of the form.
- Implements field delegates for given platform.
    - ```UITextFieldDelegate``` and ```UITextViewDelegate``` for ```UIKit```.
- Informs form delegate (most probably your VC) on state changes to take action on error or success.

---

# DEMO

