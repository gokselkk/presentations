build-lists: true
autoscale: true


# Redux
## in Swift

---

# Roadmap

- Quick MVC Discussion
- Redux in Details
- DEMO
- Overview

---

![fit](apple-mvc.png)

---

![original](mindblown.gif)

---

![fit](mvc.png)

---

> Nobody knows what MVC really is.

:see_no_evil:

---

# Problems with Apple-MVC

- View controller manages almost **everything**.
- It's not clear where **state** is and who manages it.
- Very difficult to build a **mental model** of how an application works.

:poop:

---

# :zap: Redux

---

![fit](redux.png)

---

# Counter App

![inline](counter-app.png)

---

# State

```swift
struct AppState {
    var counter: Int = 0
}
```

---

# Action

Declarative description of a state change. 

It doesn't have any implementation.

---

```swift
// Definition
struct CounterIncrementAction {
	var amount: Int
}

// Usage
@IBAction func increaseButtonTapped(sender: UIButton) {
    store.dispatch(
        CounterIncrementAction(amount: 2)
    )
}
```

---

# Reducer

The object that responds to an action which, results in a state change.

`(action, state) -> state`

---

```swift
struct CounterReducer: Reducer {
    
    func handleAction(action: Action, state: AppState?) -> AppState {
        
        var state = state ?? AppState()
        
        switch action {
        case let action as IncrementAction:
            state = handleIncrementAction(action, state: state)
        case let action as DecrementAction:
            state = handleDecrementAction(action, state: state)
        default:
            break
        }
        
        return state
    }
}

```

---

> Entire app state in one data structure.

Is this a good idea?

---

Yes. :ok_hand:

---

And no. :horse:

---

![fit](states.png)

---

> What about async operations?

---

# ActionCreator


It simply is a function that dispatches actions asynchronously.

`(state, store) -> Action?`

---

```swift
func fetchUserList() -> ActionCreator {

    return { state, store in
    
        self.apiClient.fetchUsers() { users in
        
            store.dispatch(
            	SetUsersAction(users)
            )
		}
		
		return nil
	}
}
```

---

# :red_circle: DEMO

---

# :bar_chart: Overview

---

# :-1:

- UIKit challanges.
- Encoding/decoding for time travel.
- Access to global state.

---

# :-1:

- Entirely new paradigm.
- Often API-breaking changes in framework.
- Hard to track what changes should be reflected to UI.

---

# :+1:

- Separation of concerns.
- Decoupling of intent and implementation.
- Clear, declarative API.
- Predictable, explicit state.

---

# :+1:

- Code becomes very easy to reason about.
- Automatic state propagation.
- Very light framework.
- Easy to adapt.

---

# :speech_balloon:

---

# Resources

- [Facebook's Flux](http://facebook.github.io/flux/)
- [Benji Encz: Unidirectional Data Flow in Swift](https://realm.io/news/benji-encz-unidirectional-data-flow-swift/)
- [Swift Library for Redux: ReSwift](https://github.com/ReSwift/ReSwift) 
- [ReSwift: Getting Started](http://reswift.github.io/ReSwift/master/getting-started-guide.html)

###### Göksel Köksal </br> :baby_chick: @gokselkk
