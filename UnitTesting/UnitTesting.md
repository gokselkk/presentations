
# Unit Testing :dart:

---

# :bird:

> "Bizim sektör ile futbol aşırı derecede benzerlik gösteriyor. Mesela futbol yorumcuları herşeyi biliyorlar ama takım çalıştırmıyorlar, neden? <br><br><br>**Çünkü sorumluluk var, çünkü konuşmakla yapmak farklı, çünkü adamın göZünü keserler. Herkes clean code, herkes test yazıyor, herkes biliyor...**"
-- Twitter

---

# Unit test nedir? :hatching_chick:

* Fonksiyon:

```
func sum(_ numbers: Int...) { ... }
```

* Test:

```
let actual = sum(1, 2)
let expected = 3
XCTAssertEqual(actual, expected)
```

---

![inline](meh.gif)

---

> :iphone:
<br>
> "Kullanıcının uygulamaya bir önceki girişi 24 saatten önce ise ana sayfayı tekrar yükle."

---

> :money_with_wings:
<br>
> "Kullanıcı başka bir ülkeye seyahat ettiğinde döviz kurunu bildirim olarak göster."

---

![inline](close-laptop.gif)

---

# Manuel Test

![inline](UnitTesting-ExternalFactors.png)

---

> Demo :red_circle:

