
## :iphone: iOS Software Architectures

---

# Facebook's Flux :zap:

![inline](flux-video.png)

---

# Facebook's Flux :zap:

![inline](comment1.png)
![inline](comment2.png)

---

# Facebook's Flux :zap:

![inline](comment3.png)
![inline](comment4.png)

---

# Facebook's Flux :zap:

![inline](comment5.png)

> Flux is just another MV* in disguise, but it sure is a well thought-out and well implemented one.﻿ 

---

# What's MVC really? :hatching_chick:

![inline](trygve-reenskaug.jpg)

The original MVC report is written in 1979 by 
**Trygve Reenskaug**.[^1]

[^1]: [heim.ifi.uio.no/~trygver/themes/mvc/mvc-index.html](http://heim.ifi.uio.no/~trygver/themes/mvc/mvc-index.html)

---

# What's MVC really? :hatching_chick:

> Jim Althoff and others implemented a version of MVC for the Smalltalk-80 class library after I had left Xerox PARC; I was in not involved in this work. Jim Althoff uses the term Controller somewhat differently from me.
-- Trygve Reenskaug

---

##  :iphone: Demo

---

# MVC :closed_book:

![inline](MVC.png)

---

# Apple's MVC :green_book:

![inline](AppleMVC.png)

^ Have you ever connected a UIView IBOutlet into your UIViewController subclass?

---

# Apple's MVC (Actual) :green_book:

![inline](AppleMVC-Actual.png)

---

# MVVM :blue_book:

![inline](MVVM.png)

^ Title text... Would you put it in view controller or view model?

^ Terms of use text with formatting... Would you put it in view controller or view model?

--- 

# VIPER :orange_book:

![inline](VIPER.png)

---

**MVC :closed_book: → Apple's MVC :green_book: → MVVM :blue_book: → VIPER :orange_book:**

---

# Bottom Line :books:

* MVC is controversial.
* Every architecture is a rule-set. Some has more rules than the others.
* You can mix and match architectures if necessary.
* Every choice is a trade-off and there are no silver bullets.

---

![inline](redcarpet.png)

See [github.com/gokselkoksal/RedCarpet](https://www.github.com/gokselkoksal/RedCarpet) for sample implementation.

---

![inline](avatar-circle-250.png) 

**Göksel Köksal**

![inline](github.png) [github.com/gokselkoksal](https://www.github.com/gokselkoksal)

![inline](medium.png) [medium.com/@gokselkoksal](https://www.medium.com/@gokselkoksal)

![inline](swiftpost.jpeg) [theswiftpost.co](https://theswiftpost.co/)

![inline](twitter.png) [twitter.com/gokselkk](https://www.twitter.com/gokselkk)

