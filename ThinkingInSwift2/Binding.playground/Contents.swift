//: Playground - noun: a place where people can play

import UIKit

let formatter = NumberFormatter()
formatter.numberStyle = .spellOut
let string = "This is Functional Programming"




// Long way
let splittedCharacters = string.characters.split(separator: " ");
let words = splittedCharacters.map { (characterView: String.CharacterView) -> String in
    return String(characterView)
}
let numberOfLetters = words.map { (word: String) -> Int in
    return word.characters.count
}
let numberOfLettersAsString = numberOfLetters.flatMap { (number: Int) -> String? in
    return formatter.string(from: number)
}
let formattedNumberOfLetters = numberOfLettersAsString.map { (string: String) -> String in
    return "\(string) letters"
}
let urlEncodedStrings = formattedNumberOfLetters.flatMap { (string: String) -> String? in
    return string.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
}
let urls = urlEncodedStrings.flatMap { (urlString: String) -> NSURL? in
    return NSURL(string: "https://translate.google.com/#auto/fr/\(urlString)")
}




// Short way

let translateURLs = string
    // split into words
    .characters.split(separator: " ")
    // count the number of words
    .map { $0.count }
    // convert number to string
    .flatMap { (number: Int) -> String? in return formatter.string(from: number) }
    // format number of words string
    .map { "\($0) letters" }
    // URL encode
    .flatMap { $0.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed) }
    // convert to url
    .flatMap { NSURL(string: "https://translate.google.com/#auto/fr/\($0)") }

print(translateURLs)
