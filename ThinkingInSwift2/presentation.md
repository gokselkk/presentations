
# Thinking in Swift
## #2: Functional Approach

---

## What is functional programming?

- Mathematical-style functions.
- Immutability.
- Minimum variables and state.
- Expressiveness.

--- 

## Why functional programming?

- Simple functions are easy to reason about.
- Simple functions are easy to test.
- Immutability and minimal shared state creates no side effect.
- Makes concurrency and parallel processing easier.

---

## Make use of `map` and `flatMap`

`map` and `flatMap` are simple functions that help you transform values.

```swift

// Optional<T>:

func map<U>     (transform: (T) -> U          ) -> Optional<U>
func flatMap<U> (transform: (T) -> Optional<U>) -> Optional<U>

// Array<T>:

func map<U> (transform: (T) -> U) -> [U]

```

---

## Make use of `map` and `flatMap`

```swift
// ["Goksel", "Koksal", "Monitise", "iOS"]
// ["goksel", "koksal", "monitise", "ios"]
// [<goksel.png>, <monitise.png>, <ios.png>]

let names = ["Goksel", "Koksal", "Monitise", "iOS"]
let imageNames = names.map { $0.lowercaseString }
let images = imageNames.flatMap { UIImage(name: $0) }
```

---

## Binding

"This is Functional Programming"

- Split into words

For each:

- Count the number of words
- Convert to a formatted string
- URL encode string
- Convert to URL

---

## Binding

```swift
let string = "This is Functional Programming"

let translateURLs = string
	// split into words
    .characters.split(" ")
    // count the number of words
    .map { $0.count } 
    // convert number to string
    .flatMap { formatter.stringFromNumber($0) } 
    // format number of words string
    .map { "\($0) letters" } 
    // URL encode
    .flatMap { $0.addingPercentEncodingWithAllowedCharacters(.URLFragmentAllowed()) } 
    // convert to URL
    .flatMap { NSURL(string: "https://translate.google.com/#auto/fr/\($0)") } 
   
``` 

---

## Binding

```swift
print(translateURLs)

// [
//  <https://translate.google.com/#auto/fr/this>,
//  <https://translate.google.com/#auto/fr/is>,
//  <https://translate.google.com/#auto/fr/functional>,
//  <https://translate.google.com/#auto/fr/programming>
// ]
```