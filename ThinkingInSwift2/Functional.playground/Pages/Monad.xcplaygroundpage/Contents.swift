//: Playground - noun: a place where people can play

import UIKit

enum Box<T> {
    case filled(T)
    case empty
}

extension Box {
    
    var value: T? {
        
        switch self {
        case .filled(let val):
            return val
        default:
            return nil
        }
    }
}

extension Box {
    
    func map<U>(_ transform: (T) -> U) -> Box<U> {
        
        switch self {
        case .filled(let val):
            let newValue = transform(val)
            return Box<U>.filled(newValue)
        case .empty:
            return Box<U>.empty
            
        }
    }
}

extension Box {
    
    static func flatten(box: Box<Box<T>>) -> Box<T> {
        
        switch box {
        case .filled(let innerBox):
            return innerBox
        default:
            return Box.empty
        }
    }
}

extension Box {
    
    func flatMap<U>(_ transform: (T) -> Box<U>) -> Box<U> {
        
        return Box<U>.flatten(box: map(transform))
    }
}































// -------------------------------------------
// MARK: `map` explanation
// -------------------------------------------

let nameBox = Box.filled("Goksel")
let newNameBox = nameBox.map { $0.appending(" Koksal") }
let name = newNameBox.value

// Name box - Revisited

let nameBox2 = Box.filled("Goksel")
    .map { $0.appending(" Koksal") }
    .map { $0.appending(" | CS") }































// -------------------------------------------
// MARK: `flatMap` explanation
// -------------------------------------------

let imageBox = Box.filled("Icon")
    .map { $0.lowercased() }
    .map { (imageName) -> Box<UIImage> in
        if let image = UIImage(named: imageName) {
            return Box.filled(image)
        }
        else {
            return Box.empty
        }
}

let flattenedImageBox = Box.flatten(box: imageBox)
let image = flattenedImageBox.value

// Image box - Revisited

let imageBox2 = Box.filled("Icon")
    .map { $0.lowercased() }
    .flatMap { (imageName) -> Box<UIImage> in
        if let image = UIImage(named: imageName) {
            return Box.filled(image)
        }
        else {
            return Box.empty
        }
}

let image2 = imageBox2.value































