## Routing in MVVM

---

# Problem

- Routing logic in view controllers.
- Implementing routing in reusable controllers.
- Implementing a dynamic flow.

---

# VIPER: Wireframe

![inline](VIPER.png)

---

# MVVM: Router

![inline](MVVM-Router.png)

---

# Solution

- Routing logic moved out of view controller.
- Routers can be injected to reusable controllers.
- Routers have a weak reference to view model, so it can implement any dynamic flow.

---

# RouterType

```swift
protocol RouterType {
    
    unowned var context: UIViewController { get }
    
    func showNext() -> UIViewController?
    func showPrevious()
}
```

---

## DEMO
